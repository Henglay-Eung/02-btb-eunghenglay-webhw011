import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card,Button,Form,Row,ListGroup} from 'react-bootstrap';
export default class calculator extends Component {
    constructor(){
      super();
      this.state={
        results:[],
      }
    }
    number1;
    number2;
    key=0;
    dropDownValue="+";
    getNumber1(event){
      this.number1=Number.parseInt(event.target.value);
    }
    getNumber2(event){
      this.number2=Number.parseInt(event.target.value);
    }
    getDropDownValue(event){
      this.dropDownValue=event.target.value;
    }
    calculateNumber(){
      if(isNaN(this.number1)|isNaN(this.number2))
        alert('Cannot Calculate Number')
      else{
        var result=0;
        if(this.dropDownValue==='+')
          result=this.number1+this.number2;
        else if(this.dropDownValue==='-')
          result=this.number1-this.number2;
        else if(this.dropDownValue==='*')
          result=this.number1*this.number2;
        else if(this.dropDownValue==='/')
          result=this.number1/this.number2;
        else 
          result=this.number1%this.number2;
        this.setState((prevState)=>{
          return {results:prevState.results.concat(result)}
        });

      }
    }
    render() {
        let results=this.state.results.map((result)=>(
          <Card style={{ width: '18rem' }} key={this.key++}>
            <ListGroup variant="flush">
            <ListGroup.Item>{result}</ListGroup.Item>
            </ListGroup>
            </Card>
        ));
        return (
          <div className="container">
          <Row>
            <Card style={{ width: '18rem',height:'auto'}}>
            <Card.Img variant="top" src="https://images-na.ssl-images-amazon.com/images/I/51xa4WtV1wL._SY355_.png" alt="calculator" />
            <Card.Body>
              <Form>
                <Form.Group controlId="formGroup" className="mb-4">
                  <Form.Control type="number" onChange={this.getNumber1.bind(this)}/>
                </Form.Group>
                <Form.Group controlId="formGroup" className="mb-4">
                  <Form.Control type="number" onChange={this.getNumber2.bind(this)} />
                </Form.Group> 
                <Form.Group controlId="formGridState" className="mb-4">
                  <Form.Control as="select" onChange={this.getDropDownValue.bind(this)} style={{border:'solid 1px dodgerblue',color:'dodgerblue'}}>
                    <option value="+" >+ Plus</option>
                    <option value="-">- Substract</option>
                    <option value="*">* Multiply</option>
                    <option value="/">/ Divide</option>
                    <option value="%">% Module</option>
                  </Form.Control>
                </Form.Group>
              </Form>
              <Button variant="primary" onClick={this.calculateNumber.bind(this)}>Calculate</Button>
              </Card.Body>
            </Card>
            <div className="ml-4">
              <h1>Result History</h1>
              {results}
            </div>
          </Row>

      
      
      
    </div>
        )
    }
}
